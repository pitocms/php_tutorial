<?php
	require('fpd/fpdf.php');

	$pdf = new FPDF('p','mm','A4');
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell('S',10,'INVOICE',0,1,'C');  //width,height,text,border,end of line, allign
	$pdf->SetFont('Arial','',10);
	$pdf->Cell('S',6,'Token No: UCGHJ123',0,1,'R');
	$pdf->Cell('S',6,'Created : 27-10-2017',0,1,'R');
	$pdf->Cell('S',6,'',0,1);

	$pdf->Cell(80,6,'Dhaka',0,0);
	$pdf->Cell(110,6,'Client : Kamal Khan',0,1,'R');

	$pdf->Cell(80,6,'Brance : Uttara-1',0,0);
	$pdf->Cell(110,6,'Phone : 01919000666',0,1,'R');
    
    $pdf->Cell(80,6,'Help Line : 019000 6666',0,0);
	$pdf->Cell(110,6,'Email : Kamal@example.com',0,1,'R');

	$pdf->Cell('S',6,'',0,1);
	$pdf->Cell('S',6,'',0,1);

	$pdf->Cell(80,6,'Service Started',1,0);
	$pdf->Cell(110,6,'Servicing Complete',1,1,'R');
	$pdf->Cell(80,6,'22-10-2017',0,0);
	$pdf->Cell(110,6,'27-10-2017',0,1,'R');

	$pdf->Cell('S',6,'',0,1);

	$pdf->Cell(80,6,'Service Warranty',1,0);
	$pdf->Cell(110,6,'Status',1,1,'R');
	$pdf->Cell(80,6,'Warranty',0,0);
	$pdf->Cell(110,6,'3 Months',0,1,'R');
    
    $pdf->Cell('S',6,'',0,1);

	$pdf->Cell(80,6,'Service Items',1,0);
	$pdf->Cell(110,6,'Price',1,1,'R');
	$pdf->Cell(80,6,'Laptop MotherBoard',0,0);
	$pdf->Cell(110,6,'1800 BDT',0,1,'R');
    
    $pdf->Cell('S',6,'',0,1);
	$pdf->Cell('S',6,'Total: 1800 BDT ',1,1,'R');

	$pdf->Output();
?>