<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>PHP LOGIN WITHOUT DB | AN EASY EXAMPLE</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   </head>
   <body>
      <div class="container">
         <div class="row">
         <div class="col-md-6 col-md-offset-3" style="margin-top:10%;">
         <form action="login.php" method="POST">
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" name = 'email' placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>
           <button type="submit" class="btn btn-primary">Login</button>
         </form>
         </div>
         </div>
      </div>
   </body>
</html>