<?php require_once("db.php"); ?>
<?php
	$sql = "SELECT * FROM companies";
	$result = mysqli_query($connection,$sql);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<form action="">
				 <div class="form-group">
					  <label for="sel1">Select list:</label>
					  <select class="form-control" id="sel1">
					  	<?php 
					  	  while ($row = mysqli_fetch_object($result)) {
					  	?>
					       <option><?php echo $row->name ?></option>
					    <?php }?>
					    
					  </select>
				</div> 
			</form>
		</div>
	</div>
</body>
</html>